<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses_section_build', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        DB::table('statuses_section_build')->insert([
            ['name' => 'Не началось'],
            ['name' => 'Строится'],
            ['name' => 'Сдан']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('statuses_section_build');
    }
}
