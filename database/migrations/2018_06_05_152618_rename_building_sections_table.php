<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBuildingSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('building_sections', 'building_blocks');

        Schema::table('building_blocks', function (Blueprint $table) {
            $table->string('sections')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('building_blocks', 'building_sections');

        Schema::table('building_sections', function (Blueprint $table) {
            $table->dropColumn('sections');
        });
    }
}
