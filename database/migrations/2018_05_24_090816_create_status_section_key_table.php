<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusSectionKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses_section_key', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        DB::table('statuses_section_key')->insert([
            ['name' => 'Ключи выданы'],
            ['name' => 'Идет выдача ключей'],
            ['name' => 'Начать выдачу ключей']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses_section_key');
    }
}
