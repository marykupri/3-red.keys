@extends('layouts.admin')

@section('content')
<div class="container">

    <ul>
        @foreach($buildings as $build)
        <li>{{$build->name}}</li>
        @endforeach
    </ul>
</div>

@endsection
