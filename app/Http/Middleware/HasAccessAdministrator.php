<?php

namespace App\Http\Middleware;

use DB;
use Closure;
use Illuminate\Support\Facades\Auth;

class HasAccessAdministrator
{

    public function handle($request, Closure $next) {
        $user = $request->user();

        if (! $user) {
            return redirect('/login');
        }

        $roleId = (int) $user->role_id;

        abort_if($roleId === 0, 403);

        $role = DB::table('role')->where('id', $roleId)->first();
        abort_if(! $role, 403);

        abort_if($role->code != 'administrator', 403);

        $request->session()->put('user_access', $role->code);

        return $next($request);
    }
}
