@extends('layouts.admin_mini')

@section('content')
<div class="container">
    <div class="title">Создание ЖК</div>


    <form class="form-horizontal" method="POST" action="{{ route('store') }}">
        {{ csrf_field() }}

        <div class="create_building">

            <div class="subtext">Укажите название и количество корпусов ЖК</div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="name" type="text" placeholder="Название ЖК" class="form-control" name="name" required autofocus>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="count_block" type="text" placeholder="Количество корпусов" class="form-control" name="count_block" required>
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-primary createBuild_next">
                        Далее
                    </button>
                </div>
        </div>

        <div class="create_sections" style="display:none">
            <div class="subtext">Укажите количество секций для каждого корпуса</div>

                    <input type="text" placeholder="Корпус 1" class="form-control section1" name="section[]" required>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Готово
                    </button>
                </div>

        </div>

    </form>
</div>

@endsection
