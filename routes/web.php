<?php

Auth::routes();
Route::get('/', 'HomeController@index');

/**
 * Доступ есть только у администраторов
 */
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'hasAccessAdministrator']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/create', 'HomeController@create');
    Route::post('/create', 'HomeController@store')->name('store');
});