<?php

namespace App\Http\Controllers\Admin;

use App\SectionBuildStatus;
use App\Buildings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buildings = Buildings::all();
        return view('admin.home.index', compact('buildings'));
    }

    public function create()
    {
        return view('admin.home.create');
    }

    public function store(Request $request)
    {
        Buildings::store($request->all());
    }
}
